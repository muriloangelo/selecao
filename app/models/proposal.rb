class Proposal
	attr_accessor :name, :minutes

	def initialize(name, minutes)
		@name = name
		@minutes = minutes
	end
end
