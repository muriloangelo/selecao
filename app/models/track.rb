class Track
    attr_accessor :name, :proposals
   
    def initialize(name, proposals)
        @name = name
        @proposals = select_proposal(proposals)
    end

    def select_proposal(proposals)
		@day = Time.new(2018, 7, 7, 9, 0)  	
		session_time = 180
		tracks = []
		tracks << name
		while proposals.count > 0 do
			break unless !(tracks.count > 0 && (tracks.last.include? " - Evento de Networking"))
			
			@currentProposal = proposals.sample
			@formatedHour = @day.strftime("%H:%M")

			if session_time > @currentProposal.minutes
				tracks << "#{@formatedHour} - #{@currentProposal.name}"
				@day = @day + (@currentProposal.minutes * 60)
				session_time -= @currentProposal.minutes
				proposals.delete(@currentProposal)
			elsif session_time >= 0 && session_time <= @currentProposal.minutes && @day.hour <= 12
				tracks << "12:00 - Almoço"
				@day = Time.new(2018, 7, 7, 13, 0)
				session_time = 240
			elsif session_time >= 0 && session_time <= @currentProposal.minutes && @day.hour >= 16 && @day.hour < 17
				tracks << "#{@formatedHour} - Evento de Networking"
			else
				tracks << "#{@formatedHour} - Evento de Networking"
			end
		end
		puts tracks
   end
end