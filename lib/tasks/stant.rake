namespace :stant do
  desc "BACKEND TEST"
  task backend: :environment do
  	@proposals = Array.new

	data = File.open("./storage/proposals.txt").each do |lines|
  		if !!(lines.match(/[l][i][g][h][t][n][i][n][g]/))
  			@proposals << Proposal.new(lines[0..-13], 5)
  		end

  		if !!(lines.match(/\d\d[m][i][n]/))
  			@proposals << Proposal.new(lines[0..-9], lines.match(/\d\d[m][i][n]/).to_s[0..1].to_i)
  		end
	end

	Track.new("TRACK A:", @proposals)
	Track.new("TRACK B:", @proposals)
  end
end
